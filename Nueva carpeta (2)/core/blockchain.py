from .block import Block
from hashlib import sha256
from itertools import zip_longest

class Blockchain():
    def __init__(self):
        self.blocks = []
        self.root_hash = ""
        self.block_number = 0

    def add_to_blockchain(self, data):
        if len(self.blocks) < 1:
            self.blocks.append(Block(data))
        else:
            self.block_number += 1
            if all(block.block_is_valid() for block in self.blocks):
                self.blocks.append(Block(data, self.blocks[-1].hash, self.block_number))
        self.blockchain_is_not_corrupted()


    def blockchain_is_not_corrupted(self):
        even = [block.hash for i, block in enumerate(self.blocks) if i % 2 == 0]
        odd = [block.hash for i, block in enumerate(self.blocks) if i % 2 != 0]
        temp = []
        if len(self.blocks) > 1:
            while (len(even) > 0 and len(odd) > 0):
                temp = []
                for first, sec in zip_longest(even, odd, fillvalue=""):
                    hasher = sha256()
                    tmp_hash = hasher.update(first.encode() + sec.encode())
                    temp.append(hasher.hexdigest())
                even = [block for i, block in enumerate(temp) if i % 2 == 0]
                odd = [block for i, block in enumerate(temp) if i % 2 != 0]
            self.root_hash = temp[0]
        else:
            self.root_hash = self.blocks[0].hash
        return self.root_hash
